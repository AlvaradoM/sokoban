const map = [
    "    WWWWW          ",
    "    W   W          ",
    "    WB  W          ",
    "  WWW  BWW         ",
    "  W  B B W         ",
    "WWW W WW W   WWWWWW",
    "W   W WW WWWWW  OOW",
    "W B  B          OOW",
    "WWWWW WWW WSWW  OOW",
    "    W     WWWWWWWWW",
    "    WWWWWWW        "
 ]
const main = document.getElementById("sokGame");

for (let i = 0; i < map.length; i++) {
    var row = map[i];
    var gameBoard = document.createElement("div");
    gameBoard.classList.add("currentRow");
    for (let j = 0; j < row.length; j++) {
        var cell = document.createElement("div");
        cell.dataset.rowIndex = i;
        cell.dataset.cellIndex = j;
        gameBoard.appendChild(cell);

        switch (row[j]) {
            case "W":
                cell.classList.add("wallStop");
                cell.dataset.cellType = "wall";
                break;

            case "S":
                cell.setAttribute("id", "start");
                cell.dataset.cellType = "start";
                break;

            case " ":
                cell.classList.add("walkWay");
                cell.dataset.cellType = "floor";
                break;

            case "O":
                cell.classList.add("boxHolder");
                cell.dataset.cellType = "floor";
                break;

            case "B":
                cell.classList.add("walkWay");
                cell.dataset.cellType = "floor";
                const box = document.createElement("div");
                box.classList.add("box");
                box.dataset.cellType = "box";
                cell.appendChild(box);
                break;

        }
    }
    main.appendChild(gameBoard)
}
function gameWin() {
    var winCount = 0;
    const hole = document.querySelectorAll(".boxHolder");
    hole.forEach(element => {
        var it = element.childElementCount;
        if (it === 1 && element.firstChild.id !== "monito") {
            winCount++;
        }
        if (winCount === 6) {

            setTimeout(function () {
                alert("You Win!");
            }, 500);
        }
    })
}
gameWin()

var boxTop;
var boxLeft;
var x;
var y;

const monito = document.getElementById("monito");

var start = document.getElementById("start");
start.appendChild(monito);

var myPos = start;

function getNxtPos(element, rowOffset, columnOffset) {
    const nxtRowPos = Number(element.dataset.rowIndex) + rowOffset;
    const nxtColPos = Number(element.dataset.cellIndex) + columnOffset;
    const nxtCPos = document.querySelector("[data-row-index = '" + nxtRowPos + "'][data-cell-index = '" + nxtColPos + "']");

    return nxtCPos;
}

document.addEventListener('keydown', (event) => {
    var nxtThing
    var otherThing

    switch (event.key) {

        case 'ArrowUp':
            nxtThing = getNxtPos(myPos, -1, 0)
            otherThing = getNxtPos(nxtThing, -1, 0)
            break;

        case 'ArrowDown':
            nxtThing = getNxtPos(myPos, +1, 0)
            otherThing = getNxtPos(nxtThing, +1, 0)
            break;

        case 'ArrowLeft':
            nxtThing = getNxtPos(myPos, 0, -1)
            otherThing = getNxtPos(nxtThing, 0, -1)
            break;

        case 'ArrowRight':
            nxtThing = getNxtPos(myPos, 0, +1)
            otherThing = getNxtPos(nxtThing, 0, +1)
            break;

    }


    if (nxtThing) {

        const box = nxtThing.firstElementChild;

        if (box && box.dataset.cellType === "box" &&
            otherThing.dataset.cellType === "floor" &&
            otherThing.childElementCount === 0) {

            otherThing.appendChild(box);
        }
        if (nxtThing.dataset.cellType === "floor" && nxtThing.childElementCount === 0) {

            nxtThing.appendChild(monito);
            myPos = nxtThing;

        }
    }
    gameWin();
})

