As in the maze game you worked on previously, "W" is a wall, "S" is the player's starting position, and spaces are open floor. This map also uses three new characters:

"O" is an initially empty storage location.
"B" is the starting position of a box/crate.
"X" is a storage location that starts with a box already on it.
Getting Started
Copy (or fork) your maze game from yesterday into a new repository. Do not use the same project repository.

Think about how you will represent the moving crates. Notice that both the moving crates and the moving player can be on top of either open floors or storage locations in the map. You may find it helpful to keep track of the moving items like the crates and the player separately from the static (unmoving) parts of the map (walls, floors, and storage locations).

Display the storage locations and crates. 3 points
Let the player push crates to new locations. 3 points
Prevent the player from pushing a crate if there is an obstacle beyond it (a wall or another crate). 3 points
Test for whether the player has won (all crates have been placed on top of storage locations). 1 point
Further study
Once you've gotten the basic game working, you may wish to add further maps. For example, here is a description of a more challenging map that takes many more moves to complete.